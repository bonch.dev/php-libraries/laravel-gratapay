<?php


namespace BonchDev\LaravelGratapay\Tests;


use BonchDev\LaravelGratapay\ServiceProvider;
use Orchestra\Testbench\Concerns\CreatesApplication;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set("gratapay.url", "https://gratapay.com/api/");
        $app['config']->set("gratapay.auth_key", "7f201c1d9ce90e92d084dd90955a65bf");
        $app['config']->set("gratapay.secret_key", "65cc6ab94d8b24c6bf5c580a61d58a6c");
    }

    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class
        ];
    }
}