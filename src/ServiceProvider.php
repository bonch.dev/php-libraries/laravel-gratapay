<?php


namespace BonchDev\LaravelGratapay;


use BonchDev\PHPGratapay\GratapayAPI;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/gratapay.php', 'gratapay-config'
        );

        $this->app->bind(GratapayAPI::class, function () {
            return new GratapayAPI(
                config('gratapay.url'),
                config('gratapay.auth_key'),
                config('gratapay.secret_key'),
            );
        });

    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/gratapay.php' => config_path('gratapay.php')
        ], 'gratapay-config');
    }
}