# Laravel Gratapay

Laravel service provider for php gratapay

## Install

1. `composer require bonch.dev/laravel-gratapay`
2. `php artisan vendor:publish` and find `BonchDev\LaravelGratapay\ServiceProvider` in list
3. Setup config