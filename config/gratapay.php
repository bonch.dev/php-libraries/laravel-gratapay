<?php

return [
    "url" => env("GRATAPAY_URL"),
    "auth_key" => env("GRATAPAY_AUTH_KEY"),
    "secret_key" => env("GRATAPAY_SECRET_KEY"),
];